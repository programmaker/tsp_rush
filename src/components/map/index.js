import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {toJS} from 'mobx';

import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {StyleSheet, Alert} from 'react-native';

import {getCurrentPosition, checkMarker, getMList} from '../../methods';
import Markers from '../markers';
import PolyLines from '../polylines';

@inject('stores')
@observer
class Map extends Component {
  constructor(props) {
    super(props);
    this.map = React.createRef();
    this.getPosition();
  }

  getPosition = async () => {
    if ((locationGranted = await getCurrentPosition())) this.setRegion();
  };

  componentDidUpdate() {
    const store = this.props.stores;
    if (store.ChangeRegion) this.setRegion();
  }
  setRegion() {
    const store = this.props.stores;
    try {
      setTimeout(
        () => this.mapView.animateToRegion(toJS(store.InitialRegion)),
        1000,
      );
      store.setChangeRegion(false);
    } catch (e) {
      Alert.alert(e.message || '');
    }
  }

  render() {
    const store = this.props.stores;
    return (
      <MapView
        provider={PROVIDER_GOOGLE}
        showsUserLocation
        ref={ref => {
          this.mapView = ref;
        }}
        InitialRegion={store.InitialRegion}
        onRegionChangeComplete={e => {
          if (e !== undefined) {
            try {
              store.setLatDelta(Number(e.latitudeDelta));
            } catch (err) {
              Alert.alert('', err.message);
            }
          }
        }}
        showsMyLocationButton={true}
        showsCompass={true}
        showsScale={true}
        zoomEnabled={true}
        zoomControlEnabled={true}
        loadingEnabled={true}
        style={styles.map}
        onMarkerPress={e => checkMarker(e, 'del')}
        onPress={e => checkMarker(e, 'add')}>
        <Markers />

        <PolyLines />
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  searchView: {
    marginLeft: 5,
    marginVertical: 5,
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 16,
  },
});

export default Map;
