import React, {Component} from 'react';

import {Input, Item, Icon} from 'native-base';

import {inject, observer} from 'mobx-react';

import {onSearch} from '../../methods';

@inject('stores')
@observer
class Search extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const store = this.props.stores;
    return (
      <Item>
        <Icon name="search" />
        <Input
          autoCompleteType="street-address"
          clearButtonMode="unless-editing"
          onChangeText={text => {
            store.setSearchString(text);
          }}
          onEndEditing={e => {
            onSearch(e.nativeEvent.text);
          }}
          placeholder={'Search'}
          placeholderTextColor={'gray'}
          defaultValue={store.SearchString}
        />
      </Item>
    );
  }
}

export default Search;
