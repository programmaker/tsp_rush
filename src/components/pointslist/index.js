import React, {Component} from 'react';

import {View, Content, Left, Body, Text, List, ListItem} from 'native-base';

import {inject, observer} from 'mobx-react';
import {checkMarker, cancelSearch, handlePoint} from '../../methods';

@inject('stores')
@observer
class PointsList extends Component {
  constructor(props) {
    super(props);
  }

  returnPointsList() {
    const store = this.props.stores;
    let Points = [];
    if (store.PointsList.length >= 1) {
      store.PointsList.map((val, i) =>
        Points.push(
          <ListItem
            button
            key={i}
            onPress={() => {
              handlePoint(val);
            }}>
            <Text>{val.formatted_address}</Text>
          </ListItem>,
        ),
      );
    }

    return Points;
  }

  render() {
    return (
      <View style={{height: '80%'}}>
        <Content>
          <List>{this.returnPointsList()}</List>
        </Content>
      </View>
    );
  }
}

export default PointsList;
