import React, {Component} from 'react';

import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';

import {Individual} from './individual';
import store from '../stores';
import ALGORITHM_CONFIG from './config';

class Population extends Component {
  constructor(props) {
    super(props);
  }

  // Holds individuals of population
  individuals = [];

  // Initial population of random individuals with given chromosome length
  initialize = function() {
    this.individuals = [];
    for (var i = 0; i < ALGORITHM_CONFIG.populationSize; i++) {
      var newIndividual = new Individual();
      let chromosomeLength = store.MarkersList.length;

      newIndividual.initialize(chromosomeLength);
      this.individuals.push(newIndividual);
    }
  };

  // Mutates current population
  mutate = () => {
    var fittestIndex = this.getFittestIndex();
    for (let index in this.individuals) {
      // Don't mutate if this is the elite individual and elitism is enabled
      if (ALGORITHM_CONFIG.elitism !== true || index !== fittestIndex) {
        this.individuals[index].mutate();
      }
    }
  };
  // Applies crossover to current population and returns population of offspring
  crossover = () => {
    // Create offspring population
    var newPopulation = new Population();

    // Find fittest individual
    var fittestIndex = this.getFittestIndex();
    for (let index in this.individuals) {
      // Add unchanged into next generation if this is the elite individual and elitism is enabled
      index = Number(index);
      if (ALGORITHM_CONFIG.elitism === true && index === fittestIndex) {
        // Replicate individual
        var eliteIndividual = new Individual();
        eliteIndividual.initialize(this.individuals[index].chromosome.length);
        eliteIndividual.setChromosome(
          this.individuals[index].chromosome.slice(),
        );
        newPopulation.addIndividual(eliteIndividual);
      } else {
        // Select mate
        var parent = this.tournamentSelection();
        // Apply crossover
        this.individuals[index].crossover(
          parent,
          newPopulation,
          this.individuals[index].chromosome.length,
        );
      }
    }

    return newPopulation;
  };
  // Adds an individual to current population
  addIndividual = function(individual) {
    this.individuals.push(individual);
  };
  // Selects an individual with tournament selection
  tournamentSelection = () => {
    // Randomly order population
    for (var i = 0; i < this.individuals.length; i++) {
      var randomIndex = Math.floor(Math.random() * this.individuals.length);
      var tempIndividual = this.individuals[randomIndex];
      this.individuals[randomIndex] = this.individuals[i];
      this.individuals[i] = tempIndividual;
    }
    // Create tournament population and add individuals
    var tournamentPopulation = new Population();
    for (var i = 0; i < ALGORITHM_CONFIG.tournamentSize; i++) {
      tournamentPopulation.addIndividual(this.individuals[i]);
    }
    return tournamentPopulation.getFittest();
  };

  // Return the fittest individual's population index
  getFittestIndex = () => {
    var fittestIndex = 0;
    // Loop over population looking for fittest
    for (var i = 1; i < this.individuals.length; i++) {
      if (
        this.individuals[i].calcFitness() >
        this.individuals[fittestIndex].calcFitness()
      ) {
        fittestIndex = i;
      }
    }
    return fittestIndex;
  };
  // Return fittest individual
  getFittest = () => {
    return this.individuals[this.getFittestIndex()];
  };
}

export default Population;
