/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, {Component} from 'react';
import {Provider} from 'mobx-react';
import store from './src/stores';

import Screen from './src/components/structure';

class App extends Component<{}> {
  render() {
    return (
      <Provider stores={store}>
        <Screen />
      </Provider>
    );
  }
}

export default App;
