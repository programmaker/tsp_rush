import React, {Component} from 'react';
import {Polyline} from 'react-native-maps';
import {inject, observer} from 'mobx-react';

@inject('stores')
@observer
class PolyLines extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let route = [];
    let Poly = null;
    const store = this.props.stores;

    if (store.RouteCoordinates.length !== 0) {
      store.RouteCoordinates.map((val, i) =>
        route.push({
          latitude: val.coordinate.latitude,
          longitude: val.coordinate.longitude,
        }),
      );
      Poly = <Polyline coordinates={route} />;
    } else {
      Poly = null;
    }

    return Poly;
  }
}

export default PolyLines;
