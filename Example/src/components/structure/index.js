import React, {Component} from 'react';

import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Left,
  Button,
  Text,
  Spinner,
} from 'native-base';

import Search from '../search';
import Map from '../map';
import MarkersList from '../markerslist';
import PointsList from '../pointslist';

import {getMList, cancelSearch, calculateTSP, onSearch} from '../../methods';
import {inject, observer} from 'mobx-react';
import {Alert} from 'react-native';

@inject('stores')
@observer
class Screen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const store = this.props.stores;
    return (
      <Container>
        <Header>
          <Left>
            <Search />
          </Left>
        </Header>
        <Content contentContainerStyle={{flex: 1}}>
          <Map
            ref={ref => {
              this.map = ref;
            }}
          />
          {/* {store.MarkersList.length >= 1 && !store.PointsListVisible ? (
            
          ) : null} */}
        </Content>
        {store.Calculating ? (
          <>
            <Spinner color="blue" />
          </>
        ) : store.MarkersList.length >= 1 && !store.PointsListVisible ? (
          <>
            <MarkersList />
            <Footer>
              <FooterTab>
                <Button
                  onPress={() => {
                    this.map.mapView.fitToSuppliedMarkers(getMList(), {
                      edgePadding: {top: 40, right: 10, bottom: 10, left: 10},
                    });
                  }}>
                  <Text>Fit to screen</Text>
                </Button>
              </FooterTab>
              <FooterTab>
                <Button key={'btnCalculate'} onPress={() => calculateTSP()}>
                  <Text>Calculate</Text>
                </Button>
              </FooterTab>
              <FooterTab>
                <Button key={'btnReset'} onPress={() => store.reset()}>
                  <Text>Reset</Text>
                </Button>
              </FooterTab>
            </Footer>
          </>
        ) : store.PointsListVisible ? (
          <>
            <PointsList />
            <Footer>
              <FooterTab>
                <Button
                  onPress={() => {
                    cancelSearch();
                  }}>
                  <Text>Cancel</Text>
                </Button>
              </FooterTab>

              <FooterTab>
                <Button
                  key={'btnReset'}
                  onPress={() => onSearch(store.SearchString)}>
                  <Text>Next</Text>
                </Button>
              </FooterTab>
            </Footer>
          </>
        ) : null}
      </Container>
    );
  }
}

export default Screen;
