import React, {Component} from 'react';

import {View, Content, Left, Body, Text, List, ListItem} from 'native-base';

import {inject, observer} from 'mobx-react';
import Map from '../map';
@inject('stores')
@observer
class MarkersList extends Component {
  constructor(props) {
    super(props);
  }

  returnMarkersList() {
    const store = this.props.stores;
    let Markers = [];
    if (store.Route.length === 0) {
      if (store.MarkersList.length > 0) {
        store.MarkersList.map((val, i) =>
          Markers.push(
            <ListItem
              button
              key={i}
              onPress={() => {
                store.setInitialRegion({
                  latitude: Number(val.coordinate.latitude),
                  longitude: Number(val.coordinate.longitude),
                });
                store.setLatDelta(0.0922);
                store.setChangeRegion(true);
              }}>
              <Left>
                <Text>{`Point #${i + 1}   `}</Text>
                <Body>
                  <Text>{`lat: ${val.coordinate.latitude.toFixed(
                    2,
                  )}... long: ${val.coordinate.longitude.toFixed(2)}...`}</Text>
                </Body>
              </Left>
            </ListItem>,
          ),
        );
      } else {
        Markers.push(
          <Text key="NoPoints" style={{textAlign: 'center'}}>
            No points selected
          </Text>,
        );
      }
    } else {
      Markers.push(
        <Text key="BestRoute" style={{textAlign: 'center'}}>
          The best route is:
        </Text>,
      );
      store.Route.forEach(val =>
        Markers.push(
          <ListItem
            button
            key={val}
            onPress={() => {
              store.setInitialRegion({
                latitude: store.MarkersList[val].coordinate.latitude,
                longitude: store.MarkersList[val].coordinate.longitude,
              });
              store.setLatDelta(0.0922);

              store.setChangeRegion(true);
            }}>
            <Left>
              <Text>{`Point #${val + 1}:`}</Text>

              <Body>
                <Text>{`lat: ${store.MarkersList[
                  val
                ].coordinate.latitude.toFixed(2)}... long: ${store.MarkersList[
                  val
                ].coordinate.longitude.toFixed(2)}...`}</Text>
              </Body>
            </Left>
          </ListItem>,
        ),
      );
    }

    return Markers;
  }

  render() {
    return (
      <View style={{height: '20%'}}>
        <Content>
          <List>{this.returnMarkersList()}</List>
        </Content>
      </View>
    );
  }
}

export default MarkersList;
