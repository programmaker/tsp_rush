import React, {Component} from 'react';
import {Marker} from 'react-native-maps';

import {inject, observer} from 'mobx-react';

@inject('stores')
@observer
class Markers extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const store = this.props.stores;
    if (store.MarkersList.length > 0) {
      return store.MarkersList.map((val, i) => (
        <Marker
          key={val.key}
          id={val.key}
          identifier={val.key.toString()}
          coordinate={{
            latitude: Number(val.coordinate.latitude),
            longitude: Number(val.coordinate.longitude),
          }}
        />
      ));
    } else {
      return null;
    }
  }
}

export default Markers;
