import React, {Component} from 'react';

import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';
import store from '../stores';
import ALGORITHM_CONFIG from './config';

class Individual extends Component {
  constructor(props) {
    super(props);
  }

  fitness = null;
  chromosome = [];
  // Initialize random individual
  initialize = variable => {
    this.chromosome = [];
    // Generate random chromosome
    for (var i = 0; i < store.MarkersList.length; i++) {
      this.chromosome.push(i);
    }
    for (var i = 0; i < store.MarkersList.length; i++) {
      var randomIndex = Math.floor(Math.random() * store.MarkersList.length);
      var tempNode = this.chromosome[randomIndex];
      this.chromosome[randomIndex] = this.chromosome[i];
      this.chromosome[i] = tempNode;
    }
  };

  // Set individual's chromosome
  setChromosome = function(chromosome) {
    this.chromosome = chromosome;
  };

  // Mutate individual
  mutate = () => {
    this.fitness = null;

    // Loop over chromosome making random changes
    for (let index in this.chromosome) {
      if (ALGORITHM_CONFIG.mutationRate > Math.random()) {
        var randomIndex = Math.floor(Math.random() * store.MarkersList.length);
        var tempNode = this.chromosome[randomIndex];
        this.chromosome[randomIndex] = this.chromosome[index];
        this.chromosome[index] = tempNode;
      }
    }
  };

  // Returns individuals route distance
  getDistance = () => {
    var totalDistance = 0;
    for (let index in this.chromosome) {
      var startNode = this.chromosome[index];
      var endNode = this.chromosome[0];
      if (Number(index) + 1 < this.chromosome.length) {
        endNode = this.chromosome[Number(index) + 1];
      }
      totalDistance += store.Durations[startNode][endNode];
    }

    totalDistance += store.Durations[startNode][endNode];

    return totalDistance;
  };
  // Calculates individuals fitness value
  calcFitness = () => {
    if (this.fitness != null) {
      return this.fitness;
    }

    var totalDistance = this.getDistance();
    this.fitness = 1 / totalDistance;
    return this.fitness;
  };
  // Applies crossover to current individual and mate, then adds it's offspring to given population
  crossover = function(individual, offspringPopulation, chromosomeLength) {
    var offspringChromosome = [];
    // Add a random amount of this individual's genetic information to offspring
    var startPos = Math.floor(this.chromosome.length * Math.random());
    var endPos = Math.floor(this.chromosome.length * Math.random());
    var i = startPos;
    while (i !== endPos) {
      offspringChromosome[i] = individual.chromosome[i];
      i++;
      if (i >= this.chromosome.length) {
        i = 0;
      }
    }
    // Add any remaining genetic information from individual's mate
    for (let parentIndex in individual.chromosome) {
      var node = individual.chromosome[parentIndex];
      var nodeFound = false;
      for (offspringIndex in offspringChromosome) {
        if (offspringChromosome[offspringIndex] === node) {
          nodeFound = true;
          break;
        }
      }
      if (nodeFound === false) {
        for (
          var offspringIndex = 0;
          offspringIndex < individual.chromosome.length;
          offspringIndex++
        ) {
          if (offspringChromosome[offspringIndex] === undefined) {
            offspringChromosome[offspringIndex] = node;
            break;
          }
        }
      }
    }
    // Add chromosome to offspring and add offspring to population
    var offspring = new Individual();
    offspring.initialize(
      offspringChromosome.length,
    );
    offspring.setChromosome(offspringChromosome);
    offspringPopulation.addIndividual(offspring);
  };
}

export {Individual};
