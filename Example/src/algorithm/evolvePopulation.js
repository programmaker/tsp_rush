import React, {Component} from 'react';
import ALGORITHM_CONFIG from './config';
import store from '../stores';

let evolvePopulation = (population, generationCallBack, completeCallBack) => {
  // Start evolution
  let generation = 1;
  let evolveInterval = setInterval(() => {
    if (generationCallBack !== undefined) {
      generationCallBack({
        population: population,
        generation: generation,
      });
    }
    // Evolve population
    population = population.crossover();
    population.mutate();
    generation++;

    // If max generations passed
    if (generation > ALGORITHM_CONFIG.maxGenerations) {
      // Stop looping
      clearInterval(evolveInterval);

      if (completeCallBack !== undefined) {
        completeCallBack({
          population: population,
          generation: generation,
        });
      }
    }
  }, ALGORITHM_CONFIG.tickerSpeed);
};

export {evolvePopulation};
