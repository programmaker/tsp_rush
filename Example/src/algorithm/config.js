const ALGORITHM_CONFIG = {
  crossoverRate: 0.5, //in the next releases can be modified from settings
  mutationRate: 0.1, //in the next releases can be modified from settings
  populationSize: 50, //in the next releases can be modified from settings
  tournamentSize: 5,
  elitism: true, //in the next releases can be modified from settings
  maxGenerations: 50, //in the next releases can be modified from settings
  tickerSpeed: 60,
};

export default ALGORITHM_CONFIG;
