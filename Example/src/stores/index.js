import React, {Component} from 'react';
import {observable, action, computed, extendObservable} from 'mobx';
import {observer} from 'mobx-react';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Alert} from 'react-native';

const ASPECT_RATIO = wp('100%') / hp('100%');

const DEV_API_KEY = 'AIzaSyAosJWH5-c-lsiwe9ieIKieFsi1SfuSQ2w';
const PROD_API_KEY = 'AIzaSyBWv2IGUx8Mm4131MyCrNKqBQ_dDfLjxJY';

@observer
class StoreClass extends Component {
  constructor() {
    super();
    extendObservable(this, {
      initialRegion: {
        latitude: 46.4033144095985, //default for google maps
        longitude: -1.8264053016901016, //default for google maps
        latitudeDelta: this.LatDelta,
        longitudeDelta: this.LngDelta,
      },
      key: '', // API KEY
      markersList: [], // List of existing markers
      calculating: false, //is calculating
      durations: [],
      routeCoordinates: null,
      route: null,
      pointsList: [], //search return points
      pointsListVisible: false, //search return more than one point
      searchString: null,
      pageToken: '', // page token received from google if search results more than 20 items
      changeRegion: false, //if marker or point is pressed
    });
    this.reset();
  }

  env = 'dev';
  API_KEY = this.env === 'dev' ? DEV_API_KEY : PROD_API_KEY;

  @computed get SearchString() {
    return this.searchString;
  }
  @action setSearchString(string) {
    this.searchString = string;
  }
  @computed get ChangeRegion() {
    return this.changeRegion;
  }
  @action setChangeRegion(change) {
    this.changeRegion = change;
  }

  @computed get PointsList() {
    return this.pointsList;
  }
  @action setPointsList(list) {
    if (list === null) {
      this.pointsList = [];
    } else {
      this.pointsList = list;
    }
  }
  @computed get PointsListVisible() {
    return this.pointsListVisible;
  }
  @action setPointsListVisible(state) {
    this.pointsListVisible = state;
  }
  @computed get AddressCoordinatesUrl() {
    return (
      'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?&input=' +
      this.SearchString +
      '&inputtype=textquery' +
      '&fields=formatted_address,name,geometry' +
      '&key=' +
      this.API_KEY
    );
  }

  @computed get AddressSearchUrl() {
    return (
      'https://maps.googleapis.com/maps/api/place/textsearch/json?&input=' +
      this.SearchString +
      '&pagetoken=' +
      this.pageToken +
      '&key=' +
      this.API_KEY
    );
  }

  // ONE ADDRESS
  async AddressCoordinates(url) {
    return fetch(url)
      .then(response => {
        return response.json();
      })
      .then(responseJson => {
        if (responseJson.status === 'OK') {
          return responseJson.candidates[0].geometry;
        } else {
          throw new Error(responseJson.status);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
      });
  }

  // ADDRESSES
  async AddressSearchResults(url) {
    return fetch(url)
      .then(response => {
        return response.json();
      })
      .then(responseJson => {
        if (responseJson.status === 'OK') {
          this.setPageToken(responseJson.next_page_token);
          if (responseJson.results.length === 0) {
            throw new Error('Nothing founded');
          } else if (responseJson.results.length >= 1) {
            this.setPointsListVisible(true);
          } else {
            this.setPointsListVisible(false);
          }
          return responseJson.results;
        } else {
          throw new Error(responseJson.status);
        }
      })
      .catch(error => {
        Alert.alert(error.message);
      });
  }

  @computed get PageToken() {
    return this.pageToken;
  }
  @action setPageToken(token) {
    if (token !== undefined && token !== null) {
      this.pageToken = token;
    } else {
      this.pageToken = '';
    }
  }
  LATITUDE_DELTA = 43.27334107394351; //default for google maps

  @computed get InitialRegion() {
    return this.initialRegion;
  }

  setInitialRegion(region) {
    this.initialRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: this.LatDelta,
      longitudeDelta: this.LngDelta,
    };
  }

  @computed get LngDelta() {
    return this.LatDelta * ASPECT_RATIO;
  }
  @computed get LatDelta() {
    return this.LATITUDE_DELTA;
  }
  @action setLatDelta(delta) {
    this.LATITUDE_DELTA = delta;
  }

  @computed get Key() {
    return this.key;
  }
  @action setKey(key) {
    this.key = key;
  }

  @computed get MarkersList() {
    return this.markersList;
  }
  @action setMarkersList(coordinates) {
    if (coordinates === null) {
      this.markersList = [];
    } else {
      let key = Number(this.Key);
      let mList = this.MarkersList;
      mList.push({
        coordinate: {
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
        },
        key: key,
        id: key,
        identifier: key,
      });
      key++;
      this.setKey(key);
      this.markersList = mList;
    }
  }

  @computed get Calculating() {
    return this.calculating;
  }
  @action setCalculating(calc) {
    this.calculating = calc;
  }

  @computed get Durations() {
    return this.durations;
  }
  @action setDurations(durations) {
    if (durations === null) {
      this.durations = [];
    } else {
      this.durations = durations;
    }
  }

  @computed get RouteCoordinates() {
    return this.routeCoordinates;
  }
  @action setRouteCoordinates(coordinates) {
    if (coordinates === null) {
      this.routeCoordinates = [];
    } else {
      this.routeCoordinates = coordinates;
    }
  }

  @computed get Route() {
    return this.route;
  }
  @action setRoute(coordinates) {
    if (coordinates === null) {
      this.route = [];
    } else {
      this.route = coordinates;
    }
  }

  @action reset() {
    this.setKey(0);
    this.setMarkersList(null);
    this.setCalculating(false);
    this.setDurations(null);
    this.setRouteCoordinates(null);
    this.setRoute(null);
    this.setPointsList(null);
    this.setPointsListVisible(false);
    this.setSearchString(null);
    this.setPageToken('');
  }
}

const store = new StoreClass();

export default store;
