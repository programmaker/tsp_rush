import {Alert, PermissionsAndroid} from 'react-native';

import Population from '../algorithm/population';
import {evolvePopulation} from '../algorithm/evolvePopulation';
import {toJS} from 'mobx';
import Geolocation from 'react-native-geolocation-service';
import store from '../stores';

let requestLocationPermission = async () => {
  try {
    if (Platform.OS === 'ios') {
      return true;
    } else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
      } else {
        Alert.alert('Location permission denied');
        return false;
      }
    }
  } catch (err) {
    console.warn(err);
  }
};

let getCurrentPosition = async () => {
  let granted = await requestLocationPermission();
  if (granted) {
    Geolocation.getCurrentPosition(
      position => {
        store.setLatDelta(0.0922);
        store.setInitialRegion({
          latitude: Number(position.coords.latitude),
          longitude: Number(position.coords.longitude),
        });
      },
      error => {
        error.code === 1
          ? requestLocationPermission
          : Alert.alert('', error.message);
      },
      {
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
        distanceFilter: 50,
        forceRequestLocation: true,
      },
    );
    return true;
  } else {
    Alert.alert('LOCATION NOT GRANTED');
    return false;
  }
};

let handlePoint = point => {
  if (point === null) {
    point = store.PointsList[0];
  }
  store.setInitialRegion({
    latitude: Number(point.geometry.location.lat),
    longitude: Number(point.geometry.location.lng),
  });
  store.setLatDelta(0.0922);
  store.setChangeRegion(true);
  checkMarker(point.geometry.location);
  cancelSearch();
};
/////////////// SEARCH ADDRESS STARTS ///////////////
let onSearch = async () => {
  if (store.SearchString.length !== 0) {
    let urlCoords = await store.AddressCoordinatesUrl;
    let urlAddress = await store.AddressSearchUrl;
    let results = await store.AddressSearchResults(urlAddress);

    if (results.length >= 1) {
      store.setPointsList(results);
    } else {
      Alert.alert('Something wrong with search');
    }
  }
};
/////////////// SEARCH ADDRESS ENDS ///////////////

/////////////// CANCEL SEARCH STARTS ///////////////
let cancelSearch = () => {
  store.setPointsList(null);
  store.setPointsListVisible(false);
  store.setPageToken(null);
  store.setSearchString(null);
};
/////////////// CANCEL SEARCH ENDS ///////////////

let checkMarker = (item, action) => {
  const index =
    item.nativeEvent === undefined ? NaN : parseInt(item.nativeEvent.id, 10);
  switch (action) {
    case 'add':
      if (isNaN(index)) {
        if (store.MarkersList.length === 9) {
          Alert.alert('Max amount of markers is 9');
          break;
        }
        store.setMarkersList(item.nativeEvent.coordinate);
        store.setInitialRegion({
          latitude: item.nativeEvent.coordinate.latitude,
          longitude: item.nativeEvent.coordinate.longitude,
        });
        store.setLatDelta(0.0922);
      }
      break;
    case 'del':
      if (isNaN(index)) {
        break;
      }
      for (var i = 0; i < store.MarkersList.length; i++) {
        if (
          store.MarkersList[i].coordinate.longitude ===
            item.nativeEvent.coordinate.longitude &&
          store.MarkersList[i].coordinate.latitude ===
            item.nativeEvent.coordinate.latitude
        ) {
          store.MarkersList.splice(i, 1);
        }
      }
      break;
    default:
      if (store.MarkersList.length === 9) {
        Alert.alert('Max amount of markers is 9');
        return;
      }
      if (store.MarkersList.length === 0) {
        store.setMarkersList({
          latitude: item.lat,
          longitude: item.lng,
        });
        store.setLatDelta(0.0922);
      } else {
        for (var i = 0; i < store.MarkersList.length; i++) {
          if (
            store.MarkersList[i].coordinate.longitude !== item.lng &&
            store.MarkersList[i].coordinate.latitude !== item.lat
          ) {
            store.setMarkersList({
              latitude: item.lat,
              longitude: item.lng,
            });
          }
        }
      }
      store.setLatDelta(0.0922);
      break;
  }
  store.setRoute(null);
  store.setRouteCoordinates(null);
};

/////////////// GET LIST OF EXISTING MARKERS STARTS ///////////////
let getMList = () => {
  let mList = [];
  store.MarkersList.length >= 2
    ? store.MarkersList.map(m => {
        mList.push(m.key.toString());
      })
    : null;
  return mList;
};
/////////////// GET LIST OF EXISTING MARKERS ENDS ///////////////

/////////////// CALCULATE METHODS START ///////////////
let calculateTSP = async () => {
  store.setCalculating(true);
  let durations = await getDurations();
  if (durations === undefined) {
    store.setCalculating(false);
    return;
  }
  let pop = new Population(durations);
  pop.initialize(store.MarkersList.length);
  let route = pop.getFittest().chromosome;
  evolvePopulation(
    pop,
    function(update) {
      // Get route coordinates
      route = update.population.getFittest().chromosome;
      let routeCoordinates = [];
      for (let index in route) {
        routeCoordinates[index] = store.MarkersList[route[index]];
      }
      routeCoordinates[route.length] = store.MarkersList[route[0]];
      store.setRouteCoordinates(routeCoordinates);
      store.setRoute(route);
    },
    function() {
      store.setCalculating(false);
    },
  );
};

let getDurations = async () => {
  let distanceData;
  let duration = [];
  let distanceMatrix = await getDistanceMatrix();
  for (const originIndex in distanceMatrix.rows) {
    distanceData = distanceMatrix.rows[originIndex].elements;
    duration[originIndex] = [];
    for (const destinationIndex in distanceData) {
      if (
        (duration[originIndex][destinationIndex] =
          distanceData[destinationIndex].duration === undefined)
      ) {
        Alert.alert("Error: couldn't get a trip duration from API");
        return;
      }
      duration[originIndex][destinationIndex] =
        distanceData[destinationIndex].duration.value;
    }
  }
  store.setDurations(duration);
  return duration;
};

let getDistanceMatrix = async () => {
  let url = await getDistanceMatrixURL();
  return fetch(url)
    .then(response => {
      return response.json();
    })
    .then(responseJson => {
      return responseJson;
    })
    .catch(error => {
      Alert.alert(error);
    });
};

let getDistanceMatrixURL = () => {
  let origins = '';
  let destinations = '';
  store.MarkersList.map(
    (val, i) =>
      (destinations += `${val.coordinate.latitude}%2C${val.coordinate.longitude}%7C`),
  );

  origins = destinations;

  return `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${origins}&destinations=${destinations}&key=AIzaSyAosJWH5-c-lsiwe9ieIKieFsi1SfuSQ2w`;
};
/////////////// CALCULATE METHODS END ///////////////

export {
  getCurrentPosition,
  checkMarker,
  getMList,
  onSearch,
  cancelSearch,
  calculateTSP,
  handlePoint,
};
